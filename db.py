# http://stackoverflow.com/questions/6829675/the-proper-method-for-making-a-db-connection-available-across-many-python-module/6830296#6830296

from __future__ import absolute_import
from sqlalchemy import create_engine
import config

_connection = None
_engine = None


def get_engine():
    global _engine
    if not _engine:
        _engine = create_engine(config.DB_CONNECTION)
    return _engine

# List of stuff accessible to importers of this module. Just in case
__all__ = ['getEngine']