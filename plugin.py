class Plugin(object):
    """ Plugin
    This is the base plugin class, it should be inherited by all plugins
    """

    def __init__(self, trigger_handler=None):
        """ Constructor
        trigger_handler = an instance of the Trigger Handler class
            It is used by the bot to determine when the
            "on_trigger" method is called
        """
        self.trigger_handler = trigger_handler

    def on_message(self, channel, user, message, s):
        """
        This method is called when any message is written
        """
        pass

    def on_trigger(self, channel, user, message, s):
        """
        This method is called only when the TriggerHandler is activated
        channel, user, and s are the same as above

        message = The string that comes after the trigger keyword
        """
        pass

    def on_mention(self, channel, user, message, s):
        """
        This method is called when the bots name is mentioned in a message
        """
        pass
