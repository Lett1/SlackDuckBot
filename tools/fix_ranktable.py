import db
import psycopg2
import config
import requests

if __name__ == "__main__":
    with db.get_connection() as conn:
                    with conn.cursor() as curs:
                        try:
                            curs.execute("ALTER TABLE scores ADD COLUMN id varchar(20), ADD COLUMN avatar text;")
                            curs.execute("ALTER TABLE scores DROP CONSTRAINT scores_pkey;")

                            payload = {"token": config.SLACK_TOKEN}
                            r = requests.get("https://slack.com/api/users.list", params=payload)
                            data = r.json()
                            print len(data["members"])
                            for member in data["members"]:
                                print "Fetching member %s" % member["name"]
                                curs.execute("UPDATE scores SET id = (%s), avatar= (%s) WHERE username = (%s)", [member["id"], member["profile"]["image_72"], member["name"]])

                            curs.execute("DELETE FROM scores WHERE id IS NULL;")
                            curs.execute("ALTER TABLE scores ADD PRIMARY KEY (id);")

                        except psycopg2.Error as e:
                            print e.pgerror