import db
import psycopg2

if __name__ == "__main__":
    with db.get_connection() as conn:
                    with conn.cursor() as curs:
                        try:
                            curs.execute("ALTER TABLE links DROP CONSTRAINT links_link_key;")
                            curs.execute("ALTER TABLE links ADD CONSTRAINT links_link_key UNIQUE (link, channel);")
                        except psycopg2.Error as e:
                            print e.pgerror