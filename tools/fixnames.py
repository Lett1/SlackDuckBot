# -*- coding: utf-8 -*-
import db
import psycopg2
import util
import logging

logging.basicConfig(level=logging.DEBUG, disable_existing_loggers=False)

if __name__ == "__main__":
    with db.get_connection() as conn:
                    with conn.cursor() as curs:
                        try:
                            curs.execute("SELECT * FROM textarchive;")
                            data = curs.fetchall()

                            for rowid, message in data:
                                message = util.replaceIdsInMessage(message)
                                curs.execute("UPDATE textarchive SET message = %(message)s WHERE pk = %(rowid)s;", {"message": message, "rowid": int(rowid)})
                        except psycopg2.Error as e:
                            print e.pgerror
