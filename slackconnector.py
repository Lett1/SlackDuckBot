import time
import logging
from slackclient import SlackClient


log = logging.getLogger('SlackConnector')


class SlackConnector(object):
    def __init__(self, token, type_filter=["all"]):
        self.slack = SlackClient(token)
        self.type_filter = type_filter

    def read(self):
        if self.slack.rtm_connect():
            log.info("Connected to Slack RTM API")
            done = False
            while not done:
                try:
                    events = self.slack.rtm_read()
                    for e in events:
                        if e.get("type") in self.type_filter or "all" in self.type_filter:
                            yield (e)
                        else:
                            log.debug("Ignoring event %s because filter is set for %s", e, self.type_filter)
                    time.sleep(0.1)
                except KeyboardInterrupt:
                    done = True
        else:
            print("Connection Failed, invalid token?")

    def send_msg(self, message, channel):
        log.debug("Sending Message %s to Channel %s", message, channel)
        self.slack.rtm_send_message(channel, message)

    def start_typing(self, channel):
        self.slack.server.send_to_websocket({"id": 1, "type": "typing", "channel": channel})
