import db
import logging
import math

from plugin import Plugin
from trigger_handler import TriggerHandler
from sqlalchemy import Table, Column, Integer, String, MetaData, Text


class Rank(Plugin):
    """Ranks users by their message count."""

    def __init__(self):
        super(self.__class__, self).__init__()
        
        metadata = MetaData()

        self.scores_table = Table('scores', metadata,
            Column('username', String(100)),
            Column('score', Integer, default=0),
            Column('rank', Integer),
            Column('id', String(20), primary_key=True),
            Column('avatar', Text, nullable=True),
        )

        metadata.create_all(db.get_engine())

        self.trigger_handler = TriggerHandler("rank")

    def on_trigger(self, channel, user, message, trigger, s):
        s.send_msg("Ranks can be viewed at slackrank.lett.at/" + s.slack.server.domain, channel)

    def on_message(self, channel, user, message, s):

        username = user["user"]["name"]
        slackid = user["user"]["id"]
        avatar = user["user"]["profile"]["image_72"]

        with db.get_engine().begin() as conn:

            result = conn.execute(self.scores_table.select().where(self.scores_table.c.id == slackid))
            old_entry = result.fetchone()

            # Check if the user actually exists in the table
            if old_entry:

                old_rank = 0
                if old_entry is not None:
                    old_rank = old_entry[2]

                #rank = floor(0.25 * sqrt(scores.score)),

                new_score = old_entry[1] + 1
                new_rank = math.floor(0.25 * math.sqrt(new_score))

                new_score = conn.execute(self.scores_table.update(self.scores_table.c.id == slackid).
                    values(score=new_score, rank=new_rank, username=username, avatar=avatar)).last_updated_params()

                logging.debug("new score is %s", new_score)

                if new_rank > old_rank:
                    s.send_msg("Rank Up! %s has reached Rank %d" % (username, new_rank), channel)

                # curs.execute("""INSERT INTO scores VALUES (%(username)s, 0, 0, %(id)s, %(avatar)s) ON CONFLICT (id) DO UPDATE
                #                  SET score = scores.score + 1,
                #                      rank = floor(0.25 * sqrt(scores.score)),
                #                      avatar = %(avatar)s
                #                 WHERE scores.id = %(id)s;""", {"username": username, "id": slackid, "avatar": avatar})
                # curs.execute("SELECT * FROM scores WHERE id = (%s);", (slackid,))
                # new_rank = curs.fetchone()[2]
            else:
                logging.debug("Inserting new user %s into table", username)
                conn.execute(self.scores_table.insert(), username=username, score=0, rank=0, id=slackid, avatar=avatar)
