import config
import urllib.request
from bs4 import BeautifulSoup
from plugin import Plugin
from trigger_handler import TriggerHandler

class BashOrg(Plugin):
    """Usage: `bash`. Posts a random quote from bash.org."""

    def __init__(self):
        self.trigger_handler = TriggerHandler("bash")

    def on_trigger(self, channel, user, message, trigger, s):
        print("Getting random bash quote")

        response = urllib.request.urlopen("http://www.bash.org/?random1")
        html = response.read()

        soup = BeautifulSoup(html, "lxml")
        bash_res = soup.find("p", class_="qt").text
        s.send_msg(bash_res, channel)