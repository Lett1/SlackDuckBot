import config
import util
import itertools
import db
from plugin import Plugin
from trigger_handler import TriggerHandler
from sqlalchemy import Table, Column, Integer, MetaData, Text, String
from sqlalchemy.sql import select

import logging


class CustomCommands(Plugin):
    """Usage: `commands`. Allows creation of simple commands, similiar to moobot on twitch.tv."""

    def __init__(self):
        super(self.__class__, self).__init__()
        self.own_trigger = "commands"
        self.trigger_handler = TriggerHandler(self.own_trigger)

        metadata = MetaData()

        self.customcommands_table = Table('custom_commands', metadata,
            Column('pk', Integer, primary_key=True),
            Column('trigger', String, unique=True),
            Column('message', Text)
        )

        metadata.create_all(db.get_engine())

        self.refresh_commands()

    def refresh_commands(self):
        with db.get_engine().begin() as conn:
            self.commands = conn.execute(select([
                self.customcommands_table.c.trigger,
                self.customcommands_table.c.message])).fetchall()

            self.trigger_handler.triggers = (self.own_trigger,) + tuple(x[0] for x in self.commands)
            logging.info("Command Store refreshed")

    def add_command(self, trigger, message):
        if any([x for x in self.commands if trigger in x[0]]) or trigger in list(itertools.chain.from_iterable([x.trigger_handler.triggers for x in config.PLUGINS if x.trigger_handler])):
            return ":warning: Trigger *%s* already exists! Command not added" % trigger
        else:
            with db.get_engine().begin() as conn:
                conn.execute(self.customcommands_table.insert(), trigger=trigger, message=message)
            self.refresh_commands()

            logging.debug('Added new command with message "%s" and trigger "%s"', trigger, message)
            return ':white_check_mark: Added new command with message "%s" and trigger "%s"' % (trigger, message)

    def delete_command(self, trigger):
        if any([x for x in self.commands if trigger in x[0]]):
            with db.get_engine().begin() as conn:
                conn.execute(self.customcommands_table.delete().where(self.customcommands_table.c.trigger == trigger))
            self.refresh_commands()

            logging.debug("Removed command %s", trigger)
            return ":white_check_mark: Removed command %s" % trigger
        else:
            return ":warning: Trigger *%s* does not exist!" % trigger

    def on_trigger(self, channel, user, message, trigger, s):

        if trigger == "commands":  # custom command management

            message = util.replaceUrlTags(message)
            message = util.replaceIdsInMessage(message)

            split_message = message.split(" ")
            command = split_message[0]

            if command == "list":
                if self.commands:
                    s.send_msg("Available commands:\n" +
                        "\n".join(["*%s%s*: %s" % (config.COMMAND_LEADER, x[0], x[1]) for x in self.commands]), channel)
                else:
                    s.send_msg(":disappointed: There are no custom commands defined.", channel)

            elif command == "add":
                if not user["user"]["is_admin"]:
                    s.send_msg(":no_entry_sign: You need to be admin to do that.", channel)
                    return
                if len(split_message) >= 3:
                    result = self.add_command(split_message[1], " ".join(split_message[2:]))
                    s.send_msg(result, channel)
                else:
                    s.send_msg(":x: Missing arguments, use `%scommands add <TRIGGER> <MESSAGE>` to add a new command." % config.COMMAND_LEADER, channel)

            elif command == "delete":
                if not user["user"]["is_admin"]:
                    s.send_msg(":no_entry_sign: You need to be admin to do that.", channel)
                    return
                if len(split_message) == 2:
                    result = self.delete_command(split_message[1])
                    s.send_msg(result, channel)
                else:
                    s.send_msg(":x: Missing arguments, use `%scommands delete <TRIGGER>` to remove a command." % config.COMMAND_LEADER, channel)

            else:
                s.send_msg(":warning: Available commands: `list, add*, delete*` (*needs admin rights).", channel)

        else:
            custom_command = [x for x in self.commands if trigger in x[0]]
            if custom_command:
                s.send_msg(custom_command[0][1], channel)
