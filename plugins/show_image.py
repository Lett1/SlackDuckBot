import config
import requests
from plugin import Plugin
from trigger_handler import TriggerHandler

class ShowImage(Plugin):
    """Usage: `show <TERM>`. Searches Google Images for the specified term and returns the first result. Yes, Safe-Search is on."""

    def __init__(self):
        self.trigger_handler = TriggerHandler("show")

    def google_image_search(self, query):
        url = "https://www.googleapis.com/customsearch/v1"

        payload = {"q": query, "num": 1, "start": 1, "safe": "medium", "searchType": "image", "key": config.CSE_API_KEY, "cx": config.CSE_CX}

        r = requests.get(url, params=payload)
        try:
            resp = r.json()
        except Exception:
            resp = None
        return(resp)

    def on_trigger(self, channel, user, message, trigger, s):
        search_query = message

        print("Searching bing images for %s" % search_query)

        s.start_typing(channel)

        image_search = self.google_image_search(search_query)

        if image_search is not None:
            if "items" in image_search:
                image_url = image_search["items"][0]["link"]
                s.send_msg("@%s: " % user["user"]["name"] + image_url, channel)
                return None  # Exit
        
        print("Searching failed")
        s.send_msg("@%s, I'm sorry. I couldn't find any images for that search" % user["user"]["name"], channel)