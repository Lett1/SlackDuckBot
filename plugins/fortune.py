import config
import subprocess
from plugin import Plugin
from trigger_handler import TriggerHandler
import logging


class Fortune(Plugin):
    """Usage: `fortune`. prints a random fortune fetched from the fortune command. Available on most unix/linux systems."""
    
    def __init__(self):
        self.trigger_handler = TriggerHandler("fortune")

    def on_trigger(self, channel, user, message, trigger, s):
        logging.debug("Getting random fortune from system")

        response = subprocess.check_output(["fortune"]).strip().decode(encoding='UTF-8')

        s.send_msg(response, channel)
