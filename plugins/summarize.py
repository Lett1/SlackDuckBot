import config
import re
from plugin import Plugin
from trigger_handler import TriggerHandler

from sumy.parsers.html import HtmlParser
from sumy.nlp.tokenizers import Tokenizer
from sumy.summarizers.lsa import LsaSummarizer as Summarizer
from sumy.nlp.stemmers import Stemmer
from sumy.utils import get_stop_words

LANGUAGE = "english"
SENTENCES_COUNT = 4

# You will most likely need to get the tokenizer files
# python -c "import nltk; nltk.download('punkt')"


class Summarize(Plugin):
    """Usage `summarize <URL>`. Tries to summarize the given website/article as good as possible."""

    p = re.compile(r'\<(.*?)\>')

    def __init__(self):
        self.trigger_handler = TriggerHandler("summarize")
        #todo, alias to !tldr

    def replaceUrlTags(self, message, matches):
        for match in matches:
            message = self.p.sub(match.split("|")[0], message, count=1)
        return message

    def on_trigger(self, channel, user, message, trigger, s):

        s.start_typing(channel)

        message = self.replaceUrlTags(message, re.findall(self.p, message))

        try:
            parser = HtmlParser.from_url(message, Tokenizer(LANGUAGE))
        except Exception as e:
            print(e)
            s.send_msg("Error: Please supply a valid url to summarize.", channel)
            return

        stemmer = Stemmer(LANGUAGE)

        summarizer = Summarizer(stemmer)
        summarizer.stop_words = get_stop_words(LANGUAGE)

        sentences = "\n".join([str(x) for x in summarizer(parser.document, SENTENCES_COUNT)])

        s.send_msg(sentences, channel)
