import db
import psycopg2
import re
import util

from sqlalchemy import Table, Column, Integer, MetaData, Text, String, exc
from sqlalchemy import UniqueConstraint
from plugin import Plugin


class LinkHoarder(Plugin):
    """Hoards any links posted into a database."""

    p = re.compile(r'\<(.*?)\>')

    def __init__(self):
        super(self.__class__, self).__init__()

        metadata = MetaData()

        self.links_table = Table('links', metadata,
            Column('id', Integer, primary_key=True),
            Column('link', Text, unique=True),
            Column('message', Text),
            Column('channel', String(50)),
            Column('title', Text, nullable=True),
            UniqueConstraint('link', 'channel', name='uniquelinkandchannel')
        )

        metadata.create_all(db.get_engine())

    def replaceUrlTags(self, message, matches):
        for match in matches:
            message = self.p.sub(match.split("|")[0], message, count=1)
        return message

    def on_message(self, channel, user, message, s):
        matches = re.findall(self.p, message)
        if matches:
            for match in matches:

                message = self.replaceUrlTags(message, matches)
                message = util.replaceIdsInMessage(message)

                channel_name = util.getChanneldataForId(channel)["channel"]["name"]
                url = match.split("|")[0]

                if url.startswith(("#", "@")):
                    break

                title = util.getTitle(url, timeout=5)

                with db.get_engine().begin() as conn:
                    try:
                        conn.execute(self.links_table.insert(), link=url, message=message, channel=channel_name, title=title)
                    except exc.IntegrityError as e:
                        print(e)
