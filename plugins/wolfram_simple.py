import config
from tempfile import NamedTemporaryFile
from os.path import splitext, basename, join, isfile
import requests
from plugin import Plugin
from trigger_handler import TriggerHandler
import logging
import hashlib



class WolframSimple(Plugin):
    """Usage: `wolfram`. Analyzes input using the wolframAlpha API and returns answer in text form."""
    
    def __init__(self):
        super(self.__class__, self).__init__()
        self.trigger_handler = TriggerHandler("wolfram")

        try:
            self.WOLFRAM_DIR = config.WOLFRAM_DIR
        except AttributeError as e:
            raise Exception("Missing config key WOLFRAM_DIR") from e

        try:
            self.WOLFRAM_WEB_DIR = config.WOLFRAM_WEB_DIR
        except AttributeError as e:
            raise Exception("Missing config key WOLFRAM_WEB_DIR") from e


    def on_trigger(self, channel, user, message, trigger, s):
        search_query = message

        url = "https://api.wolframalpha.com/v1/simple"
        payload = {"i": search_query, "appid": config.WOLFRAM_ALPHA_KEY}
        filename = hashlib.md5(search_query.encode('utf-8')).hexdigest()

        logging.debug("Searching WA for %s", search_query)

        s.start_typing(channel)

        if not isfile(join(self.WOLFRAM_DIR, filename + ".png")):
            r = requests.get(url, params=payload, stream=True)

            if r.status_code == requests.codes.ok:
                with open(join(self.WOLFRAM_DIR, filename + ".png"), "wb") as fd:
                    for chunk in r.iter_content(1024):
                        fd.write(chunk)

                    s.send_msg(join(self.WOLFRAM_WEB_DIR, filename + ".png"), channel)
            elif r.status_code == requests.codes.not_implemented:
                s.send_msg("@%s, Wolfram|Alpha did not understand your input"  % user["user"]["name"], channel)
            #else if r.status_code == requests.codes.unauthorized:

