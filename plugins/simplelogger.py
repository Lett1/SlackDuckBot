from __future__ import absolute_import
from __future__ import print_function
import db
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey, Text
import re
import util
from plugin import Plugin


class SimpleLogger(Plugin):
    """Logs any message to a database. For usage with the markov plugin."""

    p = re.compile(r'\<(.*?)\>')

    def __init__(self):
        super(self.__class__, self).__init__()
        
        metadata = MetaData()

        self.textarchive_table = Table('textarchive', metadata,
            Column('pk', Integer, primary_key=True),
            Column('message', Text),
        )

        metadata.create_all(db.get_engine())


    def replaceUrlTags(self, message, matches):
        for match in matches:
            message = self.p.sub(match.split("|")[0], message, count=1)
        return message


    def on_message(self, channel, user, message, s):
        matches = re.findall(self.p, message)
        if matches:
            for match in matches:
                message = self.replaceUrlTags(message, matches)

        message = util.replaceIdsInMessage(message)

        with db.get_engine().begin() as conn:
            conn.execute(self.textarchive_table.insert(), message=message)
