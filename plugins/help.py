import config
from plugin import Plugin
from inspect import getdoc
from trigger_handler import TriggerHandler


class Help(Plugin):
    """Usage: `help`. Prints help message and allows viewing help for other plugins."""

    def __init__(self):
        super(self.__class__, self).__init__()
        self.trigger_handler = TriggerHandler("help")

    def getdocstring(self, plugin):
        """Simple wrapper for getdoc to provide a default value if the docstring does not exist."""
        return getdoc(plugin) or "No docstring provided, tell the author to fix it"

    def on_trigger(self, channel, user, message, trigger, s):
        s.send_msg(":warning: To use any command, prepend it with the so called leader character, which is currently set to `%s`." % config.COMMAND_LEADER, channel)
        s.send_msg("\n".join(["*%s*: %s" % (x.__class__.__name__, self.getdocstring(x)) for x in config.PLUGINS]), channel)