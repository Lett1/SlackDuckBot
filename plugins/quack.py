from plugin import Plugin
import random

class Quack(Plugin):
    """Quacks."""

    def on_message(self, channel, user, message, s):
        if random.random() < 0.01:
            s.send_msg("Quack", channel)