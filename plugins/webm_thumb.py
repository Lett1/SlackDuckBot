import re
import config
import requests
import subprocess

from urllib.parse import urlparse
from os.path import splitext, basename, join, isfile
from plugin import Plugin
from tempfile import NamedTemporaryFile


class WebmThumb(Plugin):
    """Generates thumbnails for webm links since slack doesn't do it."""

    p = re.compile(r'\<(.*?)\>')

    def __init__(self):
        super(self.__class__, self).__init__()

        try:
            self.THUMB_DIR = config.THUMB_DIR
        except AttributeError as e:
            raise Exception("Missing config key THUMB_DIR") from e

        try:
            self.THUMB_WEB_DIR = config.THUMB_WEB_DIR
        except AttributeError as e:
            raise Exception("Missing config key THUMB_WEB_DIR") from e

    def getFilenameFromUrl(self, url):
        disassembled = urlparse(url)
        return "".join(splitext(basename(disassembled.path)))

    def replaceUrlTags(self, message, matches):
        for match in matches:
            message = self.p.sub(match.split("|")[0], message, count=1)
        return message

    def on_message(self, channel, user, message, s):
        matches = re.findall(self.p, message)
        if matches:
            for match in matches:
                url = match.split("|")[0]
                filename = self.getFilenameFromUrl(url)
                if filename.lower().endswith(".webm"):
                    if not isfile(join(self.THUMB_DIR, filename[:-4] + "png")):
                        r = requests.get(url, stream=True)

                        with NamedTemporaryFile() as fd:
                            for chunk in r.iter_content(1024):
                                fd.write(chunk)

                            thumb_file = join(self.THUMB_DIR, filename[:-4] + "png")
                            return_code = subprocess.call(['ffmpeg', '-i', fd.name, '-vf', 'thumbnail,scale=640:-1', '-frames:v', '1', thumb_file])
                            if return_code == 0:
                                s.send_msg(join(self.THUMB_WEB_DIR, filename[:-4] + "png"), channel)
                    else:
                        s.send_msg(join(self.THUMB_WEB_DIR, filename[:-4] + "png"), channel)
