from __future__ import absolute_import
import config

class TriggerHandler(object):
    """ TriggerHandler
    This class is used to abstract away the checking of a message
    """

    def __init__(self, *triggers):
        self.triggers = triggers

    def activated(self, message):
        text = message[1:]
        if message[0] != config.COMMAND_LEADER:  # This doesn't start correctly
            return False  # the trigger can't be activated
        for trigger in self.triggers:
            if text == trigger:  # But, if the text matches
                return True  # We're triggered
        return False
            
